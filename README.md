## How to build:
1. Setup your Golang (version 1.7) environment
2. Assuming that you have a valid Go setup:
```
cd $GOPATH/src
git clone git@gitlab.com:yogiB/AirplayService.git
cd AirplayService
go get github.com/gorilla/mux
go build
```

## How to start:
```
./AirplayService
```

## Usage:

The app works fine without any further input. However, ``./AirplayService -h`` will show you the cmd-line options.

There are two cmd-line options:
  * `update_interval int`
    	The interval in seconds between two updates of the currently played songs. (default 60)
  * `url string`
    	The url to get the the current Playlist from. (default "http://api.radio.de/info/v2/search/nowplayingbystations?stations=1live,swr3,antennebayern,bayern3,wdr2,ndr2,njoy,rockantenne,radiopaloma,wdr4&apikey=81e2929a541203d6fde5deb7bb4cb33c")


## How to query the service:

* In your browser: `http://localhost:8080/charts`
* On your cmd-line: `curl "http://localhost:8080/charts"`

Note: The service doesn't enforce an ordering among songs that occurred equally often. Therefore, their ordering can vary between two different queries.