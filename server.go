package main

import "encoding/json"
import "net/http"
import "log"
import "time"

import "github.com/gorilla/mux"

func Charts(w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	charts := charts_generator.GetCharts()
	encoder := json.NewEncoder(w)
	encoder.SetEscapeHTML(false)
	encoder.Encode(charts)

	log.Printf(
		"%s\t%s\t%s\t%s",
		r.Method,
		r.RequestURI,
		"charts",
		time.Since(start),
	)
}

func run_server() {
	log.Println("start server")
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/charts", Charts)
	log.Fatal(http.ListenAndServe(":8080", router))
}
