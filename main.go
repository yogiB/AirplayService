package main

import "flag"
import "log"

import "AirplayService/backend"

var flag_url string
var flag_update_interval int

var updater *backend.PeriodicUpdater
var storage backend.Storage
var charts_generator *backend.ChartsGenerator

func setup_instances() {
	log.Println("setting up backend")
	updater = backend.NewPeriodicUpdater(flag_url, flag_update_interval)
	storage = &backend.MemoryStorage{}
	go storage.UpdateFrom(updater.Start())

	charts_generator = backend.NewChartsGenerator(storage)
}

func init_flags() {
	var default_url string = "http://api.radio.de/info/v2/search/nowplayingbystations?stations=1live,swr3,antennebayern,bayern3,wdr2,ndr2,njoy,rockantenne,radiopaloma,wdr4&apikey=81e2929a541203d6fde5deb7bb4cb33c"
	flag.StringVar(&flag_url, "url", default_url, "The url to get the the current Playlist from.")
	flag.IntVar(&flag_update_interval, "update_interval", 60, "The interval in seconds between two updates of the currently played songs.")
	flag.Parse()
}

func main() {
	init_flags()
	setup_instances()

	run_server()
}
