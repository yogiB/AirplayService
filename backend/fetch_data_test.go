package backend

import "testing"

func TestEmptyUrlShouldReturnError(t *testing.T) {
	t.Parallel()
	var fetcher CurrentlyPlayedFetcher
	if _, ok := fetcher.Fetch(""); ok {
		t.Fatal("no error returned")
	}
}

func TestFetchFromValidUrlShouldSucceed(t *testing.T) {
	t.Parallel()

	var fetcher CurrentlyPlayedFetcher
	url := "http://api.radio.de/info/v2/search/nowplayingbystations?stations=1live,swr3,antennebayern,bayern3,wdr2,ndr2,njoy,rockantenne,radiopaloma,wdr4&apikey=81e2929a541203d6fde5deb7bb4cb33c"

	if _, ok := fetcher.Fetch(url); !ok {
		t.Fatal("fetch didn't succeed")
	}
}
