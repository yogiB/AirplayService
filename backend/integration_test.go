package backend

import "testing"
import "time"

func TestMemStorageShouldUpdate(t *testing.T) {
	t.Parallel()

	var pu *PeriodicUpdater = NewPeriodicUpdater("foo", 1)
	pu.fetcher = NewDummyFetcher([]byte(`{"9111": [{"streamTitle":"Wincent Weiss - Musik sein","artistName":""}]}`))
	update_chan := pu.Start()

	mem_storage := &MemoryStorage{}
	go mem_storage.UpdateFrom(update_chan)
	var cg = NewChartsGenerator(mem_storage)

	<-time.After(1500 * time.Millisecond)

	charts := cg.GetCharts()
	if size := len(charts); size != 1 {
		t.Fatalf("list length mismatch. expected: %v, got: %v", 1, size)
	}

	if charts[0] != "Wincent Weiss - Musik sein" {
		t.Fatalf("song mismatch. expected: %v, got: %v", "Wincent Weiss - Musik sein", charts[0])
	}
}
