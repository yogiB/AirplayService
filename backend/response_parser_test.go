package backend

import "testing"

var test_response_single = []byte(`{"9111": [{"streamTitle":"Wincent Weiss - Musik sein","artistName":""}]}`)
var test_response_double = []byte(`{"9111": [{"streamTitle":"Wincent Weiss - Musik sein","artistName":""}],
    "4242": [{"streamTitle":"Calvin Harris - My Way","artistName":""}]}`)

func TestReturnEmptyListOnEmptyResponse(t *testing.T) {
	t.Parallel()

	var parser ResponseParser

	if l := parser.Extract([]byte{}); len(l) != 0 {
		t.Fatal("returned nonempty list")
	}
}

func TestParseOfSingleItemReturnsCorrectNumber(t *testing.T) {
	t.Parallel()

	var parser ResponseParser

	var extracted_titles []string
	if extracted_titles = parser.Extract(test_response_single); len(extracted_titles) != 1 {
		t.Fatalf("response length mismatch. expected: %v, got: %v", 1, len(extracted_titles))
	}

	if extracted_titles = parser.Extract(test_response_double); len(extracted_titles) != 2 {
		t.Fatalf("response length mismatch. expected: %v, got: %v", 2, len(extracted_titles))
	}
}

func TestParseOfSingleItemReturnsCorrectTitle(t *testing.T) {
	t.Parallel()

	var parser ResponseParser
	var extracted_titles []string = parser.Extract(test_response_single)

	expected := "Wincent Weiss - Musik sein"
	if extracted_titles[0] != expected {
		t.Fatalf("wrong extracted title. expected: %v, got: %v", expected, extracted_titles[0])
	}

	extracted_titles = parser.Extract(test_response_double)
	expected_titles := map[string]struct{}{
		"Wincent Weiss - Musik sein": struct{}{},
		"Calvin Harris - My Way":     struct{}{},
	}

	for _, title := range extracted_titles {
		if _, exists := expected_titles[title]; !exists {
			t.Fatalf("unexpected title: %v", title)
		} else {
			delete(expected_titles, title)
		}
	}

	if len(expected_titles) > 0 {
		t.Fatalf("not all titles were extracted. missing: %v", expected_titles)
	}
}
