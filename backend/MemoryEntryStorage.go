package backend

import "time"
import "sync"
import "container/list"

type StorageEntry struct {
	time_added time.Time
	song       string
}

type MemoryStorage struct {
	time_last_update time.Time
	lock             sync.RWMutex

	all_played_songs list.List
}

func (s *MemoryStorage) GetTimeLastUpdate() time.Time {
	s.lock.RLock()
	defer s.lock.RUnlock()
	return s.time_last_update
}

func (s *MemoryStorage) GetAllEntries() []string {
	s.lock.RLock()
	defer s.lock.RUnlock()

	var l []string = make([]string, 0, s.all_played_songs.Len())

	for e := s.all_played_songs.Front(); e != nil; e = e.Next() {
		l = append(l, e.Value.(*StorageEntry).song)
	}
	return l
}

func (s *MemoryStorage) Add(songs []string) {

	s.lock.Lock()
	for i := range songs {
		new_entry := &StorageEntry{time_added: time.Now(), song: songs[i]}
		s.all_played_songs.PushBack(new_entry)
	}
	s.time_last_update = time.Now()
	s.lock.Unlock()
}

func (s *MemoryStorage) IsFrontTooOld(max_age time.Time) bool {
	var e *list.Element = s.all_played_songs.Front()
	var se *StorageEntry = e.Value.(*StorageEntry)
	return se.time_added.Before(max_age)
}

func (s *MemoryStorage) cleanup(max_age time.Duration) {
	s.lock.Lock()
	var remove_before time.Time = time.Now().Add(-max_age)
	for (s.all_played_songs.Front() != nil) && s.IsFrontTooOld(remove_before) {
		s.all_played_songs.Remove(s.all_played_songs.Front())
	}
	s.lock.Unlock()
}

func (s *MemoryStorage) UpdateFrom(update_chan chan []string) {
	for songs := range update_chan {
		s.Add(songs)
	}

	s.cleanup(24 * time.Hour)
}
