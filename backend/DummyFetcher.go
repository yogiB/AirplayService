package backend

// Mock for a fetcher
type DummyFetcher struct {
	response []byte
}

func NewDummyFetcher(response_to_return []byte) *DummyFetcher {
	return &DummyFetcher{response: response_to_return}
}

func (d *DummyFetcher) Fetch(url string) ([]byte, bool) { return d.response, true }
