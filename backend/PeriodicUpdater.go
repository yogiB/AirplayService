package backend

import "time"

func NewPeriodicUpdater(source_url string, update_interval_sec int) *PeriodicUpdater {
	pu := &PeriodicUpdater{
		source_url:      source_url,
		update_interval: time.Duration(update_interval_sec) * time.Second,
		fetcher:         &CurrentlyPlayedFetcher{},
		out_chan:        make(chan []string),
		stop_chan:       make(chan bool),
	}

	return pu
}

type PeriodicUpdater struct {
	source_url      string
	update_interval time.Duration
	fetcher         UrlFetcher
	out_chan        chan []string
	stop_chan       chan bool
}

func (pu *PeriodicUpdater) getNextList() ([]string, bool) {

	response, success := pu.fetcher.Fetch(pu.source_url)
	if !success {
		return nil, false
	}

	var parser ResponseParser
	return parser.Extract(response), true
}

func (pu *PeriodicUpdater) update() bool {
	songs, success := pu.getNextList()
	if !success {
		return false
	}
	pu.out_chan <- songs
	return true
}

func (pu *PeriodicUpdater) runUpdate() {
	ticker := time.Tick(pu.update_interval)
	for true {
		select {
		case <-ticker:
			pu.update()
		case <-pu.stop_chan:
		}
	}
}

func (pu *PeriodicUpdater) Start() chan []string {
	go pu.runUpdate()
	return pu.out_chan
}

func (pu *PeriodicUpdater) Stop() {
	pu.stop_chan <- true
}
