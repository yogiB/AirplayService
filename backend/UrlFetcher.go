package backend

type UrlFetcher interface {
	Fetch(string) ([]byte, bool)
}
