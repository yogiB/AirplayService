package backend

import "net/http"
import "io/ioutil"

type CurrentlyPlayedFetcher struct{}

func (f *CurrentlyPlayedFetcher) Fetch(url string) ([]byte, bool) {

	if len(url) == 0 {
		return nil, false
	}

	resp, err := http.Get(url)
	if err != nil {
		return nil, false
	}

	defer resp.Body.Close()
	if body, err := ioutil.ReadAll(resp.Body); err != nil {
		return nil, false
	} else {
		return body, true
	}
}
