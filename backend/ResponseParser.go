package backend

import "encoding/json"
import "fmt"

type ResponseParser struct{}

func extractFromSingleEntry(entry interface{}, out_list []string) []string {

	list, ok := entry.([]interface{})
	if !ok {
		fmt.Println("couldn't extract from entry")
		return out_list
	} else if len(list) == 0 {
		return out_list
	}

	//  the stations somewhy only have one entry in their list
	station_entry, ok := list[0].(map[string]interface{})
	if !ok {
		fmt.Println("couldn't extract from entry")
		return out_list
	}

	stream_title := station_entry["streamTitle"].(string)
	if !ok {
		fmt.Println("couldn't extract from entry")
		return out_list
	}

	out_list = append(out_list, stream_title)

	return out_list
}

func (rp *ResponseParser) Extract(text []byte) []string {
	if len(text) == 0 {
		return make([]string, 0)
	}

	var f interface{}
	if err := json.Unmarshal(text, &f); err != nil {
		fmt.Println("parsing of input failed. err: ", err)
		return make([]string, 0)
	}

	var results []string = make([]string, 0)

	m := f.(map[string]interface{})
	for _, v := range m {
		results = extractFromSingleEntry(v, results)
	}

	return results
}
