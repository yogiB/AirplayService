package backend

import "sort"
import "sync"
import "time"

type SongCounterPair struct {
	song  string
	count int
}
type ByCount []SongCounterPair

func (s ByCount) Len() int           { return len(s) }
func (s ByCount) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s ByCount) Less(i, j int) bool { return s[i].count > s[j].count }

func NewChartsGenerator(storage Storage) *ChartsGenerator {
	cg := &ChartsGenerator{
		storage: storage,
	}
	return cg
}

type ChartsGenerator struct {
	cache_mut         sync.Mutex
	cached_charts     []string
	last_cache_update time.Time

	storage Storage
}

func (cg *ChartsGenerator) countOccurrences(elements []string) map[string]int {
	var counts map[string]int = make(map[string]int)
	for i := range elements {
		if val, exists := counts[elements[i]]; exists {
			counts[elements[i]] = val + 1
		} else {
			counts[elements[i]] = 1
		}
	}
	return counts
}

func (cg *ChartsGenerator) countsToPairs(counts map[string]int) []SongCounterPair {

	pairs := make([]SongCounterPair, 0, len(counts))
	for k, v := range counts {
		pairs = append(pairs, SongCounterPair{k, v})
	}

	return pairs
}

func (cg *ChartsGenerator) pairsToStringArray(pairs []SongCounterPair) []string {
	list := make([]string, 0, len(pairs))
	for i := range pairs {
		list = append(list, pairs[i].song)
	}
	return list
}

func (cg *ChartsGenerator) isCacheUpToDate() bool {
	return cg.storage.GetTimeLastUpdate().Before(cg.last_cache_update)
}

func (cg *ChartsGenerator) GetCharts() []string {

	cg.cache_mut.Lock()
	defer cg.cache_mut.Unlock()
	if cg.isCacheUpToDate() {
		return cg.cached_charts
	}

	var entries []string = cg.storage.GetAllEntries()
	counts := cg.countOccurrences(entries)
	pairs := cg.countsToPairs(counts)
	sort.Sort(ByCount(pairs))

	cg.cached_charts = cg.pairsToStringArray(pairs)
	cg.last_cache_update = time.Now()
	return cg.cached_charts
}
