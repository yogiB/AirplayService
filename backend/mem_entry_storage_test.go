package backend

import "testing"

func TestMemStorageAddedItemsAreRetrieveable(t *testing.T) {
	t.Parallel()

	var m MemoryStorage

	songs := []string{"Calvin Harris - My Way"}
	m.Add(songs)
	all_songs := m.GetAllEntries()

	if len(all_songs) != 1 {
		t.Fatalf("length mismatch. expected: %v, got: %v", 1, len(all_songs))
	}

	if all_songs[0] != songs[0] {
		t.Fatalf("returned wrong song. expected: %v, got: %v", songs[0], all_songs[0])
	}
}

func TestMemStorageShouldUpdateLastUpdateTimer(t *testing.T) {
	t.Parallel()

	var m MemoryStorage
	m.Add([]string{"Calvin Harris - My Way"})
	if m.time_last_update.IsZero() {
		t.Fatal("internal timer isn't updated!")
	}
}

func TestMemStorageCleanUpRemovesOldEntries(t *testing.T) {
	t.Parallel()
	var m MemoryStorage

	m.Add([]string{"Calvin Harris - My Way"})
	m.cleanup(0)

	if m.all_played_songs.Len() != 0 {
		t.Fatal("old entry wasn't deleted!")
	}
}
