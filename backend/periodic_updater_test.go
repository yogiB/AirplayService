package backend

import "testing"
import "time"

func TestPerUpdaterInternalFetchShoudlSucceed(t *testing.T) {
	t.Parallel()

	var pu *PeriodicUpdater = NewPeriodicUpdater("foo", 1)
	pu.fetcher = NewDummyFetcher([]byte(`{"9111": [{"streamTitle":"Wincent Weiss - Musik sein","artistName":""}]}`))

	list, ok := pu.getNextList()
	if !ok {
		t.Fatal("update failed")
	}

	if len(list) != 1 {
		t.Fatalf("list length mismatch. expected: %v, got: %v", 1, len(list))
	}

	expected := "Wincent Weiss - Musik sein"
	if list[0] != expected {
		t.Fatalf("song mismatch. expected: %v, got: %v", expected, list[0])
	}
}

func TestPerUpdaterUpdateMustSendResult(t *testing.T) {
	t.Parallel()

	var pu *PeriodicUpdater = NewPeriodicUpdater("foo", 1)
	pu.fetcher = NewDummyFetcher([]byte(`{"9111": [{"streamTitle":"Wincent Weiss - Musik sein","artistName":""}]}`))
	pu.out_chan = make(chan []string, 1)

	ok := pu.update()
	if !ok {
		t.Fatal("update failed")
	}

	select {
	case <-pu.out_chan:
	default:
		t.Fatal("no message in the chan")
	}
}

func TestPerUpdaterUpdatesMustHappenAfterStart(t *testing.T) {

	var pu *PeriodicUpdater = NewPeriodicUpdater("foo", 1)
	pu.fetcher = NewDummyFetcher([]byte(`{"9111": [{"streamTitle":"Wincent Weiss - Musik sein","artistName":""}]}`))
	pu.out_chan = make(chan []string, 1)

	var l []string
	select {
	case l = <-pu.Start():
	case <-time.After(2 * time.Second):
		t.Fatal("no update after 2 seconds")
	}

	if len(l) == 0 {
		t.Fatal("returned list should have one entry")
	}
}
