package backend

import "time"

type Storage interface {
	GetAllEntries() []string
	UpdateFrom(chan []string)
	GetTimeLastUpdate() time.Time
}
