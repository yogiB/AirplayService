package backend

import "testing"
import "time"

// mock for Storage
type DummyStorage struct {
	entries []string
}

func (ds *DummyStorage) GetAllEntries() []string          { return ds.entries }
func (ds *DummyStorage) UpdateFrom(channel chan []string) {}
func (ds *DummyStorage) GetTimeLastUpdate() time.Time     { return time.Time{} }

func TestChartsGenShouldCreateEmptyCharts(t *testing.T) {
	t.Parallel()
	storage := &DummyStorage{entries: []string{}}
	var cg = NewChartsGenerator(storage)

	charts := cg.GetCharts()
	if size := len(charts); size != 0 {
		t.Fatalf("charts size mismatch. Expected: %v, Got: %v", 0, size)
	}
}

func TestChartsGenShouldUpdateInternalTimer(t *testing.T) {
	t.Parallel()

	storage := &DummyStorage{entries: []string{"foo"}}
	var cg = NewChartsGenerator(storage)
	compareLists(t, []string{"foo"}, cg.GetCharts())

	storage = &DummyStorage{entries: []string{"foo", "bar", "foo"}}
	cg = NewChartsGenerator(storage)
	cg.GetCharts()

	if cg.last_cache_update.IsZero() {
		t.Fatalf("internal timer for cache wasn't updated!: %v", cg.last_cache_update.String())
	}
}

func TestChartsGenShouldUpdateInternalCache(t *testing.T) {
	t.Parallel()

	storage := &DummyStorage{entries: []string{"foo"}}
	var cg = NewChartsGenerator(storage)

	cg.GetCharts()

	if len(cg.cached_charts) == 0 {
		t.Fatal("cache wasn't updated")
	}
}

func TestChartsGenShouldUseInternalCache(t *testing.T) {
	t.Parallel()

	storage := &DummyStorage{entries: []string{"foo"}}
	var cg = NewChartsGenerator(storage)

	cg.GetCharts()
	cg.cached_charts = []string{"bar"}

	compareLists(t, []string{"bar"}, cg.GetCharts())
}

func TestChartsGenShouldBuildCorrectCharts(t *testing.T) {
	t.Parallel()

	storage := &DummyStorage{entries: []string{"foo"}}
	var cg = NewChartsGenerator(storage)
	compareLists(t, []string{"foo"}, cg.GetCharts())

	storage = &DummyStorage{entries: []string{"foo", "bar", "foo"}}
	cg = NewChartsGenerator(storage)
	compareLists(t, []string{"foo", "bar"}, cg.GetCharts())

	storage = &DummyStorage{entries: []string{"foo", "bar", "foo", "s1", "s1", "s1"}}
	cg = NewChartsGenerator(storage)
	compareLists(t, []string{"s1", "foo", "bar"}, cg.GetCharts())
}

func compareLists(t *testing.T, expected []string, got []string) {

	if size := len(got); size != len(expected) {
		t.Fatalf("charts size mismatch. Expected: %v, Got: %v", len(expected), size)
	}

	for i := range expected {
		if got[i] != expected[i] {
			t.Fatalf("charts mismatch at pos %v: Expected: %v, Got: %v", i, expected[i], got[i])
		}
	}
}
